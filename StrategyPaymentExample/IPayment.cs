﻿using System;
namespace StrategyPaymentExample
{
    public interface IPayment
    {
        public void MakePayment();
    }
}
