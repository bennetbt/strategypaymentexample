﻿using System;

namespace StrategyPaymentExample
{
    class Program
    {
        static void Main(string[] args)
        {
            UserContext uc = new UserContext();
            string choice = "0";

            while (choice != "3")
            {
                Console.WriteLine("Menu Options-----");
                Console.WriteLine("1. Pay by Credit Card");
                Console.WriteLine("2. Pay by Checking Account");
                Console.WriteLine("3. Quit");
                Console.Write("---> ");
                choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        uc.SetCreditPayment();
                        break;
                    case "2":
                        uc.SetCheckPayment();
                        break;
                    case "3":
                        continue;
                }

                uc.MakePayment();
            }
        }
    }
}
