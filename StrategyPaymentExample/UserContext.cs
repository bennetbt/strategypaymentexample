﻿using System;
namespace StrategyPaymentExample
{
    public class UserContext
    {
        private string fullName
        {
            get { return fullName; }
            set { fullName = value; }
        }
        
        private string address
        {
            get { return address; }
            set { address = value; }
        }
        private string city
        {
            get { return city; }
            set { city = value; }
        }
        private string state
        {
            get { return state; }
            set { state = value; }
        }
        private string zip
        {
            get { return zip; }
            set { zip = value; }
        }
        private string telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        private IPayment payment;

        public UserContext()
        {
        }

        public void SetCheckPayment()
        {
            payment = new CheckingPayment();
        }

        public void SetCreditPayment()
        {
            payment = new CreditPayment();
        }

        public void MakePayment()
        {
            if (payment != null)
            {
                payment.MakePayment();
            }
        }

    }
}
