﻿using System;
namespace StrategyPaymentExample
{
    public class CheckingPayment : IPayment
    {
        private string bankName;
        private string routingNumber;
        private string accountNumber;

        public CheckingPayment()
        {
            bankName = "First Tennessee";
            routingNumber = "99999999";
            accountNumber = "123412341234";
        }

        void IPayment.MakePayment()
        {
            Console.Write("Enter your Routing Number: ");
            routingNumber = Console.ReadLine();

            Console.Write("Enter your Account Number: ");
            accountNumber = Console.ReadLine();

            Console.Write("Enter the Amount: ");
            Double amount = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("\n\nNow processing check payment for " + Convert.ToString(amount) + ".\n\n");
        }
    }
}
