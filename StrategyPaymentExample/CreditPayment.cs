﻿using System;
namespace StrategyPaymentExample
{
    public class CreditPayment : IPayment
    {
        private string cardType;
        private string cardNumber;
        private string cvv;
        private string expireMonth;
        private string expireYear;

        public CreditPayment()
        {
            cardType = "Visa";
            cardNumber = "0000-0000-0000-0000";
            cvv = "999";
            expireMonth = "12";
            expireYear = "20";
        }

        void IPayment.MakePayment()
        {
            Console.Write("Enter your Card Number: ");
            cardNumber = Console.ReadLine();

            Console.Write("Enter your CVV: ");
            cvv = Console.ReadLine();

            Console.Write("Enter your Expire Month: ");
            expireMonth = Console.ReadLine();

            Console.Write("Enter your Expire Year: ");
            expireYear = Console.ReadLine();

            Console.Write("Enter the Amount: ");
            Double amount = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("\n\nNow processing credit payment for " + Convert.ToString(amount) + ".\n\n");
        }
    }
}
